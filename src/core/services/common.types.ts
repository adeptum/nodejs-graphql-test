export interface AuthorsIds {
  authorsIds: ReadonlyArray<number>;
}

export interface BooksIds {
  booksIds: ReadonlyArray<number>;
}
