import { MigrationInterface, QueryRunner } from "typeorm";

export class data1643094628234 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`INSERT INTO books.authors (firstName, lastName, birthDate)
    VALUES 
           ('John', 'Capollo', '2021-05-10 19:56:28'),
           ('Andrew', 'Smith', '2021-05-12 19:56:28'),
           ('Tom', 'Moore', '2021-05-12 19:56:28')`);
    await queryRunner.query(
      `INSERT INTO books.books (name, pageCount, authorId, type, deletedAt) VALUES ('Exodus 2', 453, 1, 'Electronic', null)`
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM books.books`);
    await queryRunner.query(`DELETE FROM books.authors`);
  }
}
