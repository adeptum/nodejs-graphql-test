import { MigrationInterface, QueryRunner } from "typeorm";

export class init1643094494328 implements MigrationInterface {
  name = "init1643094494328";

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "CREATE TABLE `books` (`name` varchar(255) NOT NULL COMMENT 'Имя книги', `pageCount` int UNSIGNED NOT NULL COMMENT 'Количество страниц', `authorId` int UNSIGNED NOT NULL COMMENT 'ID автора', `type` enum ('Electronic', 'Printed') NOT NULL DEFAULT 'Printed', `bookId` int UNSIGNED NOT NULL AUTO_INCREMENT, `deletedAt` datetime(6) NULL, PRIMARY KEY (`bookId`)) ENGINE=InnoDB"
    );
    await queryRunner.query(
      "CREATE TABLE `authors` (`firstName` varchar(255) NOT NULL COMMENT 'Имя автора', `lastName` varchar(255) NOT NULL COMMENT 'Фамилия автора', `birthDate` datetime NOT NULL COMMENT 'Дата рождения автора', `authorId` int UNSIGNED NOT NULL AUTO_INCREMENT, PRIMARY KEY (`authorId`)) ENGINE=InnoDB"
    );
    await queryRunner.query(
      "ALTER TABLE `books` ADD CONSTRAINT `FK_54f49efe2dd4d2850e736e9ab86` FOREIGN KEY (`authorId`) REFERENCES `authors`(`authorId`) ON DELETE NO ACTION ON UPDATE NO ACTION"
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      "ALTER TABLE `books` DROP FOREIGN KEY `FK_54f49efe2dd4d2850e736e9ab86`"
    );
    await queryRunner.query("DROP TABLE `authors`");
    await queryRunner.query("DROP TABLE `books`");
  }
}
