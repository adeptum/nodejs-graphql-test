# Тестовое задание по NodeJS + GraphQL

В проекте есть две сущности:\
Table Книги (books);\
Table Авторы (authors);

Необходимо добавить три новые сущности в проект:\
Table Страна (country): Название (string);\
Table Категория (category): Название (string);\
Table Издатель (publisher): Название (string), Страна (country), Тип (bookType set);

Кверя автора имеет филдрезолвер на книги и страну;\
Книги имеет филдрезолвер на категорию и издателя;\
Издатель имеет филдрезолвер на страну;\
Для всех сущностей необходимо добавить хотя бы одну мутацию на создание записи;\
Для списка книг необходимо добавить любую одну фильтрацию в input по типу книги или категории;\
Если будет желание, через репозиторий можешь сделать мутацию на удаление записи;

Таким образом мы сможем выбрать из базы список авторов, все книги по нему и так далее в одной квере;\
Это задание поможет научиться создавать entity с колонкой типа enum, а также с колонкой типа set (когда можно выбрать более одного значения из enum);\
Обрати внимание, что граф будет падать пока не сделаешь registerEnumType каждого енума, подробнее в самом низу сообщения;\
Также ты погрузишься и научишься делать кверибилдеры и даталоадеры для филдрезолверов;

Еще прикрепляю небольшую выдержку из нашего readme, будет полезно понять общепринятые правила нейминга и тд.

# Библиотеки

Их нужно знать.

- typeorm - ORM
  - работа с репозиторием (не active record),
  - работа с "QueryBuilder"
  - работа с миграциями
- tsyringe - DI
- type-graphql - GraphQl
  - Resolver
  - FieldResolver
  - ObjectType
  - ObjectInput
  - Args
- dataloader - fix graphql problem N+1
- pm2 - runner
- nodemon - dev runner
- config - configuration (nmp config)
  - default.ts
  - local.ts
- yarn workspaces - monorepo

# Соглашения по именованию методов

- add\* - добавить связь (без создания новой сущности)\
  Неправильно: link
- remove\* - удалить связь (без удаления сущности)\
  Неправильно: unlink
- save\* - создать, обновить сущность\
  Неправильно: add, create, update, change.
- drop\* - удалить сущность\
  Неправильно: delete, remove.
- findBy, findGoodBy, findOneGoodBy - поиск любой сущности по любым параметрам\
  Неправильно: findAbcForCdeWithFgh, getValueFromDbAndSaveToBrain
  Если метод бросает ошибку в случае не найденой сущности, назавние должно начинаться с get
- Обозначение списка сущностей.\
  goodId - один айди\
  goodsIds - список айдишников\
  Неправильно: goodIds, goodsId, id, ids.

# Имена файлов

Имена файлов записываются через тире с маленькой буквы. Допускается использовать точки для категорий например some.entity.ts \
Кодировка utf-8. \
Не верно: ConsignorsReadme.txt \
Верно: consignors-readme.txt

# Комментарии.

Новые функции должны сопровождаться комментарием в стиле jsdoc. Если функция выполняет бизнес требования нужно на него сослаться.

# Структура файлов

Каждый резолвер находится в своей папке.\
В папке находятся файлы: {resolver-name}.gq с резолвером и {resolver-name}.gq-types с input\output типами

# Типизация полей

Для каждого поля непримитивного типа необходимо указывать его тип в декораторе.

```
@Field(() => DeliveryState, {
  description: 'Состояние доставки транспортной накладной',
  nullable: true,
})
@Column(...)
deliveryState: DeliveryState;
```

Перечисляемые типы (enum), используемые в GraphQL схеме, необходимо регистрировать, добавив к наименованию префикс Gq

```
registerEnumType(DeliveryState, {
  name: 'GqDeliveryState',
  description: 'Состояние доставки',
});
```

# Abstract Query Builder

Используется как абстрактный класс для имплементации queryBuilder-a отдельно взятой сущности.

# Запуск проекта

## Запуск докера для DB:

```
docker-compose up --build -d
```

## Первоначальная настройка проекта:

```
yarn install
yarn build
yarn typeorm migration:run
```

## Запуск проекта:

```
yarn start
```

## Создание миграции:

```
yarn typeorm migration:generate -n "init"
```

## Запуск миграций в DB:

```
yarn typeorm migration:run
```

# Существующие методы GraphQL:

```
query books {
  books(input: { booksIds: [1] }) {
    bookId
    name
    pageCount
    authorId
    author {
      authorId
      firstName
      lastName
      birthDate
    }
    type
  }
}
```

```
mutation saveAuthor {
  saveAuthor(
    input: {
      firstName: "Вячеслав"
      lastName: "коростелев"
      birthDate: "1987-09-18"
    }
  ) {
    authorId
  }
}
```

```
mutation saveBook {
  saveBook(
    input: {
      name: "Звездные войны"
      pageCount: 99
      authorId: 1
      type: Printed
    }
  ) {
    bookId
  }
}
```

# Итоговые методы, которые должны работать:

По этому адресу расположен готовый запущенный проект, в котором все нижеследующее реализовано, можно использовать для сравнения API со своим:
http://graphql.adeptum.ru/graphql

```
query books {
  books(input: { booksIds: [1] }) {
    bookId
    name
    pageCount
    authorId
    author {
      authorId
      firstName
      lastName
      birthDate
    }
    categoryId
    category {
      categoryId
      name
    }
    type
    publisherId
    publisher {
      publisherId
      name
      countryId
      country {
        countryId
        name
      }
      type
    }
  }
}
```

```
mutation saveAuthor {
  saveAuthor(
    input: {
      firstName: "Вячеслав"
      lastName: "коростелев"
      birthDate: "1987-09-18"
    }
  ) {
    authorId
  }
}
```

```
mutation saveCategory {
  saveCategory(input: { name: "Фантастика" }) {
    categoryId
  }
}
```

```
mutation saveCountry {
  saveCountry(input: { name: "Россия" }) {
    countryId
  }
}
```

```
mutation savePublisher {
  savePublisher(input: { name: "Адептум", countryId: 1, type: Printed }) {
    publisherId
  }
}
```

```
mutation saveBook {
  saveBook(
    input: {
      name: "Звездные войны"
      pageCount: 99
      authorId: 1
      categoryId: 1
      type: Printed
      publisherId: 1
    }
  ) {
    bookId
  }
}
```
